module.exports =  {
    parser:  '@typescript-eslint/parser',
    extends:  [
      'prettier',
      'plugin:@typescript-eslint/recommended', 
      'plugin:react/recommended',
      'prettier/@typescript-eslint',
      'plugin:import/warnings',
      'plugin:import/typescript',
      'plugin:prettier/recommended'
    ],
    plugins: [
      "prettier"
    ],
    parserOptions:  {
      ecmaVersion:  2018,
      sourceType:  'module',
      ecmaFeatures:  {
        jsx:  true,
      },
    },
    rules:  {
      "@typescript-eslint/no-unused-vars": [
        "warn",
        {
          "argsIgnorePattern": "^_",
          "vars": "all",
          "ignoreRestSiblings": true
        }
      ],


      "@typescript-eslint/no-inferrable-types": "warn",
      "@typescript-eslint/no-empty-interface":  [
        "warn",
        {
          "allowSingleExtends": true
        }
      ],
      "@typescript-eslint/interface-name-prefix": "warn",
      "@typescript-eslint/explicit-member-accessibility": "off",
      "@typescript-eslint/prefer-for-of": "off",
      "@typescript-eslint/explicit-function-return-type": "off",
      "@typescript-eslint/no-explicit-any": "off",
      "@typescript-eslint/no-var-requires": "off",
      "@typescript-eslint/camelcase": "off",
      "@typescript-eslint/no-use-before-define": "off",

      "complexity": ["warn", 25],
      "max-lines": ["warn", 400],
      "sort-keys": "off",
      "object-shorthand": ["warn", "properties"],
      "one-var": "off",
      "prefer-rest-params": "warn",

      "react/jsx-no-bind": "off",
      "react/jsx-boolean-value": "off",
      "react/jsx-no-multiline-js": "off",
      "react/prop-types": "off",
      "react/no-children-prop": "off",

      "react/display-name": "off",
      "react/jsx-no-target-blank": "off",
      "react/jsx-key": "warn",

      "import/no-duplicates": "off",
    },
    settings:  {
      react:  {
        version:  '16.8.6',
      },
    },
  };
