const AssetsPlugin = require('assets-webpack-plugin');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { TsconfigPathsPlugin } = require('tsconfig-paths-webpack-plugin');
const { InjectManifest } = require('workbox-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const BrotliPlugin = require('brotli-webpack-plugin');

const IS_STATIC_BUILD = process.env.BUILD_TYPE === 'static';
const IS_SERVER_BUILD = process.env.BUILD_TYPE === 'server';
const IS_PROD = process.env.NODE_ENV === 'production';

const ClientWebpack = {
    entry: {
        app: './src/client/index.tsx'
    },
    output: {
        filename: '[name]__[hash].js',
        path: path.join(__dirname, './dist')
    },
    module: {
        rules: [{ test: /\.tsx?$/, loader: 'ts-loader' }]
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js'],
        plugins: [
            new TsconfigPathsPlugin({
                configFile: path.relative(__dirname, 'tsconfig.json')
            })
        ]
    },
    optimization: {
        mergeDuplicateChunks: true,
        splitChunks: {
            name: 'vendors',
            chunks: 'all'
        }
    },
    plugins: [
        new CleanWebpackPlugin(),
        new InjectManifest({
            swSrc: path.join('src', 'sw.js'),
            importWorkboxFrom: 'disabled',
            precacheManifestFilename: 'sw-chunks.[manifestHash].js'
        }),
        new AssetsPlugin(),
        new BrotliPlugin({
            asset: '[path].br[query]',
            algorithm: 'brotli',
            test: /\.(js|css|html|svg)$/,
            threshold: 1200,
            minRatio: 0.8,
            quality: 11
        })
    ]
};

const ServerWebpack = {
    target: 'node',
    entry: {
        server: './src/server/index.ts'
    },
    output: {
        filename: '[name].js',
        path: path.join(__dirname, './dist')
    },
    module: {
        rules: [{ test: /\.tsx?$/, loader: 'ts-loader' }]
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js'],
        plugins: [
            new TsconfigPathsPlugin({
                configFile: path.relative(__dirname, 'tsconfig.node.json')
            })
        ]
    },
    plugins: [new TerserPlugin()]
};

const StaticWebpack = {
    entry: {
        app: './src/client/index.tsx'
    },
    output: {
        filename: '[name]__[hash].js',
        path: path.join(__dirname, './dist')
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js'],
        plugins: [
            new TsconfigPathsPlugin({
                configFile: path.relative(__dirname, 'tsconfig.json')
            })
        ]
    },
    module: {
        rules: [{ test: /\.tsx?$/, loader: 'ts-loader' }]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new InjectManifest({
            swSrc: path.join('src', 'sw.js'),
            importWorkboxFrom: 'disabled',
            precacheManifestFilename: 'sw-chunks.[manifestHash].js'
        }),
        new HtmlWebpackPlugin({
            template: './public/index.js',
            inject: false
        }),
        new CopyPlugin([{ from: './public/_redirects' }])
    ],
    optimization: {
        mergeDuplicateChunks: true,
        splitChunks: {
            name: 'vendors',
            chunks: 'all'
        }
    },
    ...(IS_PROD
        ? {
              devServer: {
                  historyApiFallback: true,
                  //   open: true,
                  //   http2: true,
                  //   compress: true,
                  stats: {
                      children: false,
                      maxModules: 0
                  },
                  port: 3000,
                  clientLogLevel: 'silent'
              }
          }
        : {})
};

module.exports = IS_STATIC_BUILD ? StaticWebpack : IS_SERVER_BUILD ? ServerWebpack : ClientWebpack;
