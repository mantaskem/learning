module.exports = (templateParams) => {
    const filteredScripts = templateParams.htmlWebpackPlugin.files.js.filter(
        (item) => !item.startsWith('sw')
    );
    const scripts = `${filteredScripts.map(
        (item) => `<script type="text/javascript" src="${item}" async></script>`
    )}`.replace(/,/g, '');
    return `
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset='utf-8'>
            <meta http-equiv='X-UA-Compatible' content='IE=edge'>
            <title>Mantas Kemėšius</title>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
            ${scripts}
        </head>
        <body>
            <div id="app"></div>
        </body>
    </html>
    `;
};
