import { Thunk } from 'utils/redux';
import { AppState } from 'store/reducer';
import { registerServiceWorker } from 'features/serviceWorker/thunks';

export const initializeListeners = (): Thunk<AppState> => (dispatch) => {
    window.addEventListener('load', () => {
        dispatch(registerServiceWorker());
    });
};
