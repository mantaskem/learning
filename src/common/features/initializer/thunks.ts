import { Thunk } from 'utils/redux';
import { AppState } from 'store/reducer';
import { initializeListeners } from 'features/listeners/thunks';
import { initializeAnalytics } from 'features/analytics/thunks';

export const initializeClientSide = (): Thunk<AppState> => (dispatch) => {
    dispatch(initializeAnalytics());
    dispatch(initializeListeners());
};
