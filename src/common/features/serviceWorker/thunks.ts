import { AppState } from 'common/store/reducer';
import { Thunk } from 'common/utils/redux';

export const registerServiceWorker = (): Thunk<AppState> => () => {
    navigator.serviceWorker
        .register('/sw.js')
        .catch((err: string) => console.error(`Service worker (Error): ${err}`));
};
