import trackStructEvents from 'features/analytics/struct';
import trackUnstructEvents from 'features/analytics/unstruct';
import { Action } from 'utils/redux';
import { AppState } from 'store/reducer';
import { ThunkMiddleware } from 'redux-thunk';
import { initializeAnalytics } from 'features/analytics/thunks';

const middleware = () => {
    const analyticsMiddleware: ThunkMiddleware<AppState> = ({ dispatch }) => (next) => (
        action: Action<any>
    ) => {
        const returnValue = next(action);
        dispatch(initializeAnalytics());
        dispatch(trackStructEvents(action));
        dispatch(trackUnstructEvents(action));

        return returnValue;
    };

    return analyticsMiddleware;
};

export default middleware;
