import { combineReducers } from 'redux';
import { setLoggingEnabled, setInitialized } from 'features/analytics/actions';
import { createReducer, set, reduce } from '@reduxify/utils';

interface TrackingState {
    isLoggerActive: boolean;
    isInitialized: boolean;
}

const defaultState: TrackingState = {
    isLoggerActive: false,
    isInitialized: false
};

const isLoggerActive = createReducer(defaultState.isLoggerActive, reduce(setLoggingEnabled, set));

const isInitialized = createReducer(
    defaultState.isInitialized,
    reduce(setInitialized, () => true)
);

export default combineReducers<TrackingState>({
    isLoggerActive,
    isInitialized
});
