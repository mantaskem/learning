import ReactGA from 'react-ga';
import { Thunk, Action } from 'utils/redux';
import { AppState } from 'store/reducer';
import { History } from 'history';

const trackUnstructEvents = (action: Action<any>): Thunk<AppState> => (dispatch) => {
    dispatch(trackPageChanges(action as Action<History>));
    // dispatch(trackModalChanges(action));
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const trackPageChanges = (action: Action<History>): Thunk<AppState> => (dispatch, getState) => {
    const state = getState();

    if (action.type !== '@@router/LOCATION_CHANGE') {
        return;
    }

    if (!action.payload) {
        console.error('Action payload missing');
        return;
    }

    ReactGA.pageview(action.payload.location.pathname);

    if (state.analytics.isLoggerActive) {
        logger(`PAGE: ${action.payload} CHANGE`);
    }
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const trackModalChanges = (action: Action<string>): Thunk<AppState> => (dispatch, getState) => {
    const state = getState();

    if (!action.payload) {
        console.error('Action payload missing');
        return;
    }

    ReactGA.modalview(action.payload);
    if (state.analytics.isLoggerActive) {
        logger(`MODAL: ${action.payload} OPENED`);
    }

    ReactGA.modalview('MODAL_CLOSED');
    if (state.analytics.isLoggerActive) {
        logger('MODAL CLOSED');
    }
};

const logger = (message: string): Thunk<AppState> => () => {
    console.log('UNSTRUCT EVENT', message);
};

export default trackUnstructEvents;
