export interface CollectorReturnProps {
    category: string;
    action: string;
    label?: string;
    value?: number;
}
