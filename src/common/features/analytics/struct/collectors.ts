import { CATEGORY, ACTION } from 'features/analytics/struct/constants';
import { AppState } from 'store/reducer';
import { Action } from 'utils/redux';
import { CollectorReturnProps } from 'features/analytics/types';

export const contentImpressionDataCollector = (
    _: AppState,
    { payload }: Action<string>
): CollectorReturnProps => ({
    category: CATEGORY.impression,
    action: ACTION.Seen,
    label: payload
});
