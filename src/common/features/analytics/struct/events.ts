import { contentImpressionDataCollector } from 'features/analytics/struct/collectors';
import { AppState } from 'store/reducer';
import { Action } from 'utils/redux';
import { EventArgs } from 'react-ga';

type TrackingCollector = (state: AppState, payload: Action<any>) => EventArgs;

const STRUCT_EVENTS: Record<string, TrackingCollector> = {
    ['SUDAS']: contentImpressionDataCollector
};

export default STRUCT_EVENTS;
