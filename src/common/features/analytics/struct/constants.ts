export enum CATEGORY {
    Page = 'PAGE',
    PageVisibility = 'PAGE_VISIBILITY',
    impression = 'IMPRESSION',
    User = 'USER'
}

export enum ACTION {
    Seen = 'SEEN',
    Change = 'CHANGE',
    Unload = 'UNLOAD',
    Authorization = 'AUTHORIZATION',
    FB_AUTH = 'FACEBOOK_AUTHORIZATION'
}

export enum LABEL {
    Visible = 'VISIBLE',
    Hidden = 'HIDDEN',
    Success = 'SUCCESS',
    Unsuccess = 'UNSUCCESS'
}
