import { AppState } from 'store/reducer';
import { Action, Thunk } from 'utils/redux';
import Events from 'features/analytics/struct/events';
import ReactGA from 'react-ga';
import { CollectorReturnProps } from 'features/analytics/types';

const trackStructEvents = (action: Action<any>): Thunk<AppState> => (dispatch, getState) => {
    const state = getState();
    if (Events[action.type]) {
        ReactGA.event(Events[action.type](state, action));
        if (state.analytics.isLoggerActive) {
            dispatch(logger(Events[action.type](state, action)));
        }
    }
};

const logger = (eventData: CollectorReturnProps): Thunk<AppState> => () => {
    console.log('STRUCT EVENT: ', eventData);
};

export default trackStructEvents;
