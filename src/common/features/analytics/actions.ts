import { createActionCreator } from '@reduxify/utils';

export const setInitialized = createActionCreator('TRACKING/SET_INITIALIZED');
export const setLoggingEnabled = createActionCreator<boolean>('TRACKING/SET_LOGGING_ENABLED');
