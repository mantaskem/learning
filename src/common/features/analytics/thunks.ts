import ReactGA from 'react-ga';
import { Thunk } from 'utils/redux';
import { AppState } from 'store/reducer';
import { setLoggingEnabled, setInitialized } from 'features/analytics/actions';
import { getCookie } from 'utils/cookie';
import { config } from 'utils/config';

export const initializeAnalytics = (): Thunk<AppState> => (dispatch, getState) => {
    if (getState().analytics.isInitialized) {
        return;
    }
    dispatch(setInitialized());

    if (!!getCookie('t')) {
        dispatch(setLoggingEnabled(config.isDev));
    }
    ReactGA.initialize(config.gaId);
};
