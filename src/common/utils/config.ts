export interface AppConfig {
    isDev: boolean;
    url: string;
    apiUrl: string;
    fbId: string;
    gaId: string;
    env: string;
}

// tslint:disable: no-string-literal

const productionConfig = {
    url: '',
    apiUrl: '',
    fbId: '2378926535653056',
    gaId: 'UA-155044425-1',
    gaViewId: '155044425'
};

export const config: AppConfig = {
    env: process.env.NODE_ENV || 'development',
    isDev: process.env.NODE_ENV !== 'production',
    url: 'http://localhost:3000',
    apiUrl: '',
    fbId: '373854863326459',
    gaId: 'UA-155044425-1',
    ...(process.env.NODE_ENV === 'production' ? productionConfig : {})
};
