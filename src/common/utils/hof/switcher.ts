import * as React from 'react';

type Selector<T> = (props: T) => React.ComponentType<any>;

/**
 * HOF to switch between which component to render
 */
const switcher = <T = any>(selector: Selector<T>) => {
    const Switcher: React.FC<T> = (props) => React.createElement(selector(props), props);

    return Switcher;
};

export default switcher;
