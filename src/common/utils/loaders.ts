export const initializeLoaders = () => {
    tryRegisterServiceWorker();
};

const tryRegisterServiceWorker = () => {
    window.addEventListener('load', () => {
        navigator.serviceWorker
            .register('/sw.js')
            .catch((err: string) => console.error(`Service worker (Error): ${err}`));
    });
};
