import { ThunkDispatch, ThunkAction } from 'redux-thunk';
import { AnyAction } from 'redux';

export type Dispatch<S = {}> = ThunkDispatch<S, undefined, AnyAction>;
export type GenericThunk<T, S> = ThunkAction<T, S, void, AnyAction>;
export type Thunk<S> = GenericThunk<void, S>;
export interface Action<T> {
    type: string;
    payload?: T;
}

export type Reducer<S, P> = (state: S, action: Action<P>) => S;
