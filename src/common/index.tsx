import * as React from 'react';
import { connect } from 'react-redux';
import { useEffect } from 'react';
import { Route, Switch } from 'react-router';
import { Dispatch } from 'utils/redux';
import { initializeClientSide } from 'features/initializer/thunks';
import { AppState } from 'common/store/reducer';
import { ThemeProvider } from 'styled-components';
import NotFound from 'common/pages/notFound/Component';
import { renderRoutes } from 'common/pages/routes';

interface Props {
    onMount: () => void;
}

const App: React.FC<Props> = ({ onMount }) => {
    useEffect(() => onMount());

    return (
        <ThemeProvider theme={{ mode: 'dark' }}>
            <Switch>
                {renderRoutes()}
                <Route component={NotFound} />
            </Switch>
        </ThemeProvider>
    );
};

const mapDispatchToProps = (dispatch: Dispatch<AppState>) => ({
    onMount: () => dispatch(initializeClientSide())
});

export default connect(null, mapDispatchToProps)(App);
