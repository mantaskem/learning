import { applyMiddleware, createStore, DeepPartial } from 'redux';
import thunk from 'redux-thunk';
import { routerMiddleware } from 'connected-react-router';
import { AppState, createRootReducer } from 'store/reducer';
import { composeWithDevTools } from 'redux-devtools-extension';
import { History } from 'history';
import analyticsMiddleware from 'features/analytics/middleware';

export default function configureStore(history: History, preloadedState: DeepPartial<AppState>) {
    const store = createStore(
        createRootReducer(history),
        preloadedState,
        composeWithDevTools(
            applyMiddleware(routerMiddleware(history), thunk, analyticsMiddleware())
        )
    );

    return store;
}
