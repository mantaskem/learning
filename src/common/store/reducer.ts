import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { History } from 'history';
import analytics from 'features/analytics/reducer';

export const createRootReducer = (history: History) =>
    combineReducers({
        router: connectRouter(history),
        analytics
    });

export type AppState = ReturnType<ReturnType<typeof createRootReducer>>;
