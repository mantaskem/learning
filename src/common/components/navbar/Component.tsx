import * as React from 'react';
import { Link } from 'react-router-dom';
import { LADING_PAGE, ADMIN_PAGE } from 'common/pages/routes';

const NavBar = () => (
    <div>
        <Link to={LADING_PAGE}>Home</Link>
        <Link to={ADMIN_PAGE}>Admin</Link>
    </div>
);

export default NavBar;
