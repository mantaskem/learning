import * as React from 'react';
import styled from 'styled-components';

interface Props {
    type?: ButtonType;
    size?: ButtonSize;
    onClick: () => void;
}

export enum ButtonType {
    DEFAULT = 'default'
}

export enum ButtonSize {
    SMALL = 'small'
}

const StyledButton = styled.button`
    cursor: pointer;
    background: transparent;
    font-size: 16px;
    border-radius: 3px;
    color: palevioletred;
    border: 2px solid palevioletred;
    padding: 0.25em 1em;
    &:hover {
        background-color: palevioletred;
        color: white;
    }
`;

const Button: React.FC<Props> = ({
    onClick,
    children,
    type = ButtonType.DEFAULT,
    size = ButtonSize.SMALL
}) => <StyledButton onClick={onClick}>{children}</StyledButton>;

export default Button;
