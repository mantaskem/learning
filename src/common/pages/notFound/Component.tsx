import * as React from 'react';
import NavBar from 'common/components/navbar/Component';

const NotFound: React.FC = () => (
    <div>
        <NavBar />
        <h1>404 Not found!</h1>
    </div>
);

export default NotFound;
