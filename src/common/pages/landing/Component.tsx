import * as React from 'react';
import Button from 'common/components/button/button';
import NavBar from 'common/components/navbar/Component';

const LandingPage: React.FC = () => (
    <div>
        <NavBar />
        <h1>Landing page</h1>
        <Button onClick={() => console.log('mantas ir cia')}>mantas</Button>
    </div>
);

export default LandingPage;
