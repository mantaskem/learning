import * as React from 'react';
import { matchPath, match } from 'react-router-dom';
import LandingPage from 'common/pages/landing/Component';
import AdminPage from 'common/pages/admin/Component';
import { Route } from 'react-router';

export const LADING_PAGE = '/';
export const ADMIN_PAGE = '/admin';

interface RouteProps {
    path: string;
    match: (url: string) => match | null;
    exact: boolean;
    component: React.FC;
}

export const routes: Record<string, RouteProps> = {
    landing: {
        path: LADING_PAGE,
        match: (url: string) => matchPath(url, { path: LADING_PAGE, exact: true }),
        exact: true,
        component: LandingPage
    },
    admin: {
        path: ADMIN_PAGE,
        match: (url: string) => matchPath(url, { path: ADMIN_PAGE, exact: true }),
        exact: true,
        component: AdminPage
    }
};

export const renderRoutes = () =>
    Object.keys(routes).map((key) => {
        const { path, component, exact } = routes[key];
        return <Route key={path} path={path} component={component} exact={exact} />;
    });
