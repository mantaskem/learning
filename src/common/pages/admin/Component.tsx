import * as React from 'react';
import NavBar from 'common/components/navbar/Component';

const AdminPage: React.FC = () => (
    <div>
        <NavBar />
        <h1>Admin page</h1>
    </div>
);

export default AdminPage;
