const pages = ['/', '/admin'];
const cacheAssets = [...[].concat(self.__precacheManifest || []).map((item) => item.url), ...pages];
const vendorsFileName = cacheAssets.filter((item) => item.startsWith('vendors'))[0];
const cacheName = vendorsFileName.substring(9, vendorsFileName.length - 3);

self.addEventListener('install', (e) => {
    e.waitUntil(
        caches
            .open(cacheName)
            .then((cache) => {
                cache.addAll(cacheAssets);
            })
            .then(() => self.skipWaiting())
    );
});

self.addEventListener('activate', (e) => {
    e.waitUntil(
        caches.keys().then((cacheNames) =>
            Promise.all(
                cacheNames.map((cache) => {
                    if (cache !== cacheName) {
                        return caches.delete(cache);
                    }
                })
            )
        )
    );
});

self.addEventListener('fetch', (e) =>
    e.respondWith(fetch(e.request).catch(() => caches.match(e.request)))
);
