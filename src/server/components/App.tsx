import React from 'react';
import { Store } from 'redux';
import { AppState } from 'common/store/reducer';
import { StaticRouter } from 'react-router';
import { Provider } from 'react-redux';
import CommonApp from 'common';

interface Props {
    store: Store<AppState>;
    scripts: string[];
    styles: React.ReactElement<{}>[];
    location: string;
}

const createPreloadedState = (store: Store<AppState>) =>
    `window.__PRELOADED_STATE__ = ${JSON.stringify(store.getState())}`;

const App: React.FC<Props> = ({ store, scripts, styles, location }) => (
    <html>
        <head>
            <meta charSet="utf-8" />
            <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
            <meta
                name="viewport"
                content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"
            />
            <meta
                name="description"
                content="Mantas Kemėšius SSR REACT, REDUX, TYPESCRIPT PROJECT"
            />
            <title>Mantas Kemėšius</title>
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            {styles}
            <script
                type="module"
                async={true}
                dangerouslySetInnerHTML={{ __html: createPreloadedState(store) }}
            />
            {scripts.map((script) => (
                <script key={script} src={script} async={true} type="module" />
            ))}
        </head>
        <body>
            <div id="app">
                <StaticRouter location={location}>
                    <Provider store={store}>
                        <CommonApp />
                    </Provider>
                </StaticRouter>
            </div>
        </body>
    </html>
);

export default App;
