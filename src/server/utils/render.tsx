import * as React from 'react';
import { createMemoryHistory } from 'history';
import configureStore from 'common/store/configureStore';
import { renderToString } from 'react-dom/server';
import { Request } from 'express';
import { Store } from 'redux';
import App from 'server/components/App';
import { ServerStyleSheet } from 'styled-components';
import CommonApp from 'common';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { AppState } from 'common/store/reducer';

const initializeApp = (store: Store) => {
    //do something (fetching and etc...)
    return store;
};

export const getServerSideApp = (req: Request) => {
    const store = createStoreWithData(req);
    const scripts = getScripts();
    const styles = getStyles(store, req.url);

    return renderToString(
        <App store={store} scripts={scripts} styles={styles} location={req.url} />
    );
};

const createStoreWithData = (req: Request) =>
    initializeApp(configureStore(createMemoryHistory({ initialEntries: [req.url] }), {}));

const getScripts = () => {
    const assets = getWebpackAssets();
    const assetKeys = Object.keys(assets);

    if (!assetKeys.length) {
        return [];
    }

    return assetKeys.filter((key) => key).map((key: string) => assets[key].js);
};

const getWebpackAssets = () => {
    try {
        return require('../../../webpack-assets.json');
    } catch {
        return {};
    }
};

const getStyles = (store: Store<AppState>, location: string) => {
    const sheet = new ServerStyleSheet();
    renderToString(
        sheet.collectStyles(
            <StaticRouter location={location}>
                <Provider store={store}>
                    <CommonApp />
                </Provider>
            </StaticRouter>
        )
    );

    return sheet.getStyleElement();
};
