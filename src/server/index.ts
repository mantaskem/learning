import express from 'express';
import { getServerSideApp } from 'server/utils/render';
import path from 'path';
import compression from 'compression';

const server = express();
const port = 3000;
// frontend/dist -> because in production is used frontend dir.
const assetsPath =
    process.env.NODE_ENV === 'production' ? path.resolve('frontend/dist') : path.resolve('dist');

server.use(compression());
server.use(express.static(assetsPath));

server.get('*', (req, res) => res.send(getServerSideApp(req)));
server.listen(port, () => console.log(`Listening on port: ${port}`));
