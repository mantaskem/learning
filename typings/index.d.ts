declare interface Window {
    __PRELOADED_STATE__?: any;
    __REDUX_DEVTOOLS_EXTENSION__?: () => <T>(createStore: T) => T;
}
